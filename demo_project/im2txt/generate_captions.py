import os
from django.conf import settings
import subprocess


def generate_captions(image_name):
    im2txt_dir = os.path.join(settings.BASE_DIR, 'im2txt')
    MODEL_DIR = "./im2txt/train/"

    CHECKPOINT_PATH = im2txt_dir + "/model/train/"
    VOCAB_FILE = im2txt_dir + "/word_counts.txt"
    IMAGE_FILE = os.path.join(settings.MEDIA_ROOT, image_name)

    print(IMAGE_FILE)

    #str_cuda = "export CUDA_VISIBLE_DEVICES = ''"
    #os.system(str_cuda)

    nic_gen = im2txt_dir + "/bazel-bin/im2txt/run_inference" + " --checkpoint_path=" + CHECKPOINT_PATH + " --vocab_file=" \
          + VOCAB_FILE + " --input_files=" + IMAGE_FILE


    print(nic_gen)

    # os.system(gen)

    nic_caps = subprocess.check_output(nic_gen, shell=True)
    print(nic_caps)
    captions = nic_caps.decode('utf-8').split("\n")[1:4]
    print(captions)

    return captions
