MSCOCO_DIR="/Volumes/O/im2txt/data/mscoco"
MODEL_DIR="./im2txt/train/"

CHECKPOINT_PATH="./model/train/"
VOCAB_FILE="./word_counts.txt"
IMAGE_FILE=$1

bazel build -c opt //im2txt:run_inference

export CUDA_VISIBLE_DEVICES=""

bazel-bin/im2txt/run_inference --checkpoint_path=${CHECKPOINT_PATH} --vocab_file=${VOCAB_FILE} --input_files=${IMAGE_FILE}


