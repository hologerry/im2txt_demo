var myApp = angular.module("FrontendApp", [
    'ngResource',
    'ngFileUpload',
    'chart.js'
]);

myApp.config(function ($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
});

myApp.controller('MainCtrl', function ($scope, Images)
{
    console.log('In main Control');

    /**
     * An array containing all the images shown on the page
     * @type {Array}
     */
    $scope.images = [];
    $scope.images_size = 0;


    $scope.newImage = {};

    $scope.dataset = [];
    $scope.data = [];

    $scope.labelset = [];
    $scope.label = [];


    /**
     * Indicating whether images are being loaded or not
     * @type {boolean}
     */
    $scope.imagesLoading = false;

    /**
     * Load images from API
     * @returns {*}
     */

    $scope.loadImages = function() {
        // console.log("loading images ------------------");
        $scope.imagesLoading = true;

        return Images.query().$promise.then(
            // on success
            function success(response) {
                // store response
                $scope.images = response;

                $scope.loadDatas();

                $scope.imagesLoading = false;

                return response;
            },
            // on error
            function error(rejection) {
                // log the error to console
                console.log(rejection);
                // loading has finished (although with an error)
                $scope.imagesLoading = false;
                return rejection;
            }
        );
    };


    /**
     * Upload an Image on Button Press
     */
    $scope.uploadImage = function()
    {
        // call REST API endpoint
        Images.save($scope.newImage).$promise.then(
            function(response) {
                // the response is a valid image, put it at the front of the images array
                console.log("uploaded image -----");
                $scope.images.unshift(response);

                // reset newImage
                $scope.newImage = {};


                console.log("refreshing__up_")
                // $scope.loadImages();
                // Windows.reload();
                // $scope.loadImages();
                $scope.loadDatas();
            },
            function(rejection) {
                console.log('Failed to upload image');
                console.log(rejection);
            }
        );
    };

    $scope.deleteImage = function (image) {
        image.$delete(
            function (response) {
                // success delete
                console.log('Deleted it');

                // update $scope.images
                var idx = $scope.images.indexOf(image);
                if (idx < 0) {
                    console.log('Error: Could not find image');
                } else {
                    $scope.images.splice(idx, 1);
                    console.log("refreshing__delete_")
                    $scope.loadDatas();
                }
            },
            function(rejection)
            {
                // failed to delete it
                console.log('Failed to delete image');
                console.log(rejection);
            }
        );
    };

    $scope.loadDatas = function() {
        console.log("loading data ------------------");

        $scope.dataset = [];

        $scope.labelset = [];


        $scope.images_size = $scope.images.length;


        for (i = 0; i < $scope.images_size; i++) {
            // console.log(i);
            $scope.label = [];
            $scope.data = [];

            $scope.label.push($scope.images[i].mil_att0);
            $scope.data.push(parseFloat($scope.images[i].mil_att0_w));
            $scope.label.push($scope.images[i].mil_att1);
            $scope.data.push(parseFloat($scope.images[i].mil_att1_w));
            $scope.label.push($scope.images[i].mil_att2);
            $scope.data.push(parseFloat($scope.images[i].mil_att2_w));
            $scope.label.push($scope.images[i].mil_att3);
            $scope.data.push(parseFloat($scope.images[i].mil_att3_w));
            $scope.label.push($scope.images[i].mil_att4);
            $scope.data.push(parseFloat($scope.images[i].mil_att4_w));
            $scope.label.push($scope.images[i].mil_att5);
            $scope.data.push(parseFloat($scope.images[i].mil_att5_w));
            $scope.label.push($scope.images[i].mil_att6);
            $scope.data.push(parseFloat($scope.images[i].mil_att6_w));
            $scope.label.push($scope.images[i].mil_att7);
            $scope.data.push(parseFloat($scope.images[i].mil_att7_w));
            $scope.label.push($scope.images[i].mil_att8);
            $scope.data.push(parseFloat($scope.images[i].mil_att8_w));
            $scope.label.push($scope.images[i].mil_att9);
            $scope.data.push(parseFloat($scope.images[i].mil_att9_w));

            // console.log("data");
            // console.log($scope.data);
            // console.log("label");
            // console.log($scope.label);



            $scope.dataset.push($scope.data);
            $scope.labelset.push($scope.label);

        }
        console.log("dataset");
        console.log( $scope.dataset);
        console.log("labelset");
        console.log($scope.labelset);
        // loading has finished
    };



    $scope.loadImages();
});
