from rest_framework import serializers
from image_upload.models import UploadedImage


class UploadImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = UploadedImage

        fields = ('pk', 'image', 'thumbnail', 'title', 'nic_cap0', 'nic_cap1', 'nic_cap2',
                  'mil_att0', 'mil_att1', 'mil_att2', 'mil_att3', 'mil_att4', 'mil_att5',
                  'mil_att6', 'mil_att7', 'mil_att8', 'mil_att9',
                  'mil_att0_w', 'mil_att1_w', 'mil_att2_w', 'mil_att3_w', 'mil_att4_w', 'mil_att5_w',
                  'mil_att6_w', 'mil_att7_w', 'mil_att8_w', 'mil_att9_w',
                  )
        read_only_fields = ('thumbnail', 'nic_cap0', 'nic_cap1', 'nic_cap2',
                            'mil_att0', 'mil_att1', 'mil_att2', 'mil_att3', 'mil_att4', 'mil_att5',
                            'mil_att6', 'mil_att7', 'mil_att8', 'mil_att9',
                            'mil_att0_w', 'mil_att1_w', 'mil_att2_w', 'mil_att3_w', 'mil_att4_w', 'mil_att5_w',
                            'mil_att6_w', 'mil_att7_w', 'mil_att8_w', 'mil_att9_w',
                            )

