import uuid
import os
from django.db import models
from django_mysql.models import ListCharField

from PIL import Image
from django.conf import settings

from im2txt.generate_captions import generate_captions
from mil.test import generate_atts


def scramble_uploaded_filename(instance, filename):
    extension = filename.split('.')[-1]
    random_name = filename.split('.')[0]
    return "{}.{}".format(random_name, extension)


# creates a thumbnail of an existing image
def create_thumbnail(input_image, thumbnail_size=(380, 380)):
    # make sure an image has been set
    if not input_image or input_image == "":
        return

    # open image
    image = Image.open(input_image)

    # use PILs thumbnail method; use anti aliasing to make the scaled picture look good
    image.thumbnail(thumbnail_size, Image.ANTIALIAS)

    # parse the filename and scramble it
    filename = scramble_uploaded_filename(None, os.path.basename(input_image.name))
    arrdata = filename.split(".")
    # extension is in the last element, pop it
    extension = arrdata.pop()
    basename = "".join(arrdata)
    # add _thumb to the filename
    new_filename = basename + "_thumb." + extension

    # save the image in MEDIA_ROOT and return the filename
    image.save(os.path.join(settings.MEDIA_ROOT, new_filename))

    return new_filename


class UploadedImage(models.Model):
    title = models.CharField("Title of the uploaded image", max_length=255, default="Picture")
    image = models.ImageField("Upload image", upload_to=scramble_uploaded_filename)
    thumbnail = models.ImageField("Thumbnail of uploaded image", blank=True)

    # Google Neural Image Caption generator
    # nic_captions = ListCharField(base_field=models.CharField(max_length=512), size=3, max_length=(3*513), default="")
    nic_cap0 = models.CharField("NIC caption 0", max_length=100, default="cap")
    nic_cap1 = models.CharField("NIC caption 1", max_length=100, default="cap")
    nic_cap2 = models.CharField("NIC caption 2", max_length=100, default="cap")

    # Multiple-Instance Learning
    # mil_atts = ListCharField(base_field=models.CharField(max_length=10), size=10, max_length=(10*11), default="")
    # mil_atts_w = ListCharField(base_field=models.CharField(max_length=8), size=10, max_length=(10*11), default="")
    mil_att0 = models.CharField("MIL attribute 0", max_length=20, default="att")
    mil_att0_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    mil_att1 = models.CharField("MIL attribute 1", max_length=20, default="att")
    mil_att1_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    mil_att2 = models.CharField("MIL attribute 2", max_length=20, default="att")
    mil_att2_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    mil_att3 = models.CharField("MIL attribute 3", max_length=20, default="att")
    mil_att3_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    mil_att4 = models.CharField("MIL attribute 4", max_length=20, default="att")
    mil_att4_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    mil_att5 = models.CharField("MIL attribute 5", max_length=20, default="att")
    mil_att5_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    mil_att6 = models.CharField("MIL attribute 6", max_length=20, default="att")
    mil_att6_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    mil_att7 = models.CharField("MIL attribute 7", max_length=20, default="att")
    mil_att7_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    mil_att8 = models.CharField("MIL attribute 8", max_length=20, default="att")
    mil_att8_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    mil_att9 = models.CharField("MIL attribute 9", max_length=20, default="att")
    mil_att9_w = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)

    def __str__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # generate and set thumbnail or none
        self.thumbnail = create_thumbnail(self.image)
        super(UploadedImage, self).save(force_update=force_update)
        
        nic_captions = generate_captions(self.image.name)
        mil_atts, mil_atts_w = generate_atts(self.image.name)

        # self.nic_captions = nic_captions
        self.nic_cap0, self.nic_cap1, self.nic_cap2 = nic_captions
        print(self.nic_cap0)

        self.mil_att0, self.mil_att1, self.mil_att2, self.mil_att3, self.mil_att4, self.mil_att5, \
            self.mil_att6, self.mil_att7, self.mil_att8, self.mil_att9 = mil_atts
        self.mil_att0_w, self.mil_att1_w, self.mil_att2_w, self.mil_att3_w, self.mil_att4_w, self.mil_att5_w, \
                self.mil_att6_w, self.mil_att7_w, self.mil_att8_w, self.mil_att9_w = mil_atts_w
        self.mil_atts = mil_atts
        self.mil_atts_w = mil_atts_w
        print(mil_atts)
        print(mil_atts_w)

        super(UploadedImage, self).save(force_update=force_update)
