# Generated by Django 2.0.4 on 2018-04-23 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('image_upload', '0006_auto_20180320_1227'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploadedimage',
            name='mil_atts',
            field=models.CharField(default='', max_length=512, verbose_name='Attribute 0'),
        ),
        migrations.AddField(
            model_name='uploadedimage',
            name='mil_atts_w',
            field=models.CharField(default='', max_length=512, verbose_name='Attr_Weight 0'),
        ),
        migrations.AlterField(
            model_name='uploadedimage',
            name='caption1',
            field=models.CharField(default='', max_length=512, verbose_name='Caption 0:'),
        ),
        migrations.AlterField(
            model_name='uploadedimage',
            name='caption2',
            field=models.CharField(default='', max_length=512, verbose_name='Caption 1:'),
        ),
        migrations.AlterField(
            model_name='uploadedimage',
            name='caption3',
            field=models.CharField(default='', max_length=512, verbose_name='Caption 2:'),
        ),
    ]
